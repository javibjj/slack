#!/bin/bash

# Setting variables
SUCCESS=0
EXIT_STATUS=$1
###################################################################

# Checking the environment
if [[ "${ENVIRONMENT}" == "Production" ]]; then        
        SLACK_WEBHOOK=$prod_channel
    else        
        SLACK_WEBHOOK=$stage_channel
    fi
###################################################################

# Commit message format - "message : @mentions"
# Example - "Tested and deployed the code: @Javib.jone95"
if [[ $CI_COMMIT_MESSAGE =~ ['@'] ]]; then
    assign=`echo $CI_COMMIT_MESSAGE | sed 's/.*://'`
	commit_msg=`echo $CI_COMMIT_MESSAGE | sed 's/:.*//'`
else
    assign=""
	commit_msg=$CI_COMMIT_MESSAGE
fi
###################################################################
###################################################################

function print_slack_summary() {

        local slack_msg_header
        local slack_msg_body
        local slack_channel

# Condition for build jobs:

	if [[ "${job_name}" == "build" ]]; then
		if [[ "${EXIT_STATUS}" != "${SUCCESS}" ]]; then
        slack_msg_header=":x: *Build in ${ENVIRONMENT} failed*"
		slack_msg_body="*Build info:*"
		assign=""
		fi

# Condition for rollback jobs:		

	elif [[ "${job_name}" == "rollback" ]]; then
		if [[ "${EXIT_STATUS}" == "${SUCCESS}" ]]; then
			slack_msg_header=":white_check_mark: *Rollback succeeded in ${ENVIRONMENT}*"
			slack_msg_body="*Rollback info:*"
		else
			slack_msg_header=":x: *Rollback failed in ${ENVIRONMENT}*"
			slack_msg_body="*Rollback info:*"
		fi

# Condition for deployment jobs:

	else	
		if [[ "${EXIT_STATUS}" == "${SUCCESS}" ]]; then
			slack_msg_header=":white_check_mark: *Deploy to ${ENVIRONMENT} succeeded*"
			slack_msg_body="*Deployment info:*"
		else
			slack_msg_header=":x: *Deploy to ${ENVIRONMENT} failed*"
			slack_msg_body="*Rollback info:*"	
		fi
	fi
		

# JSON blocks for printing in slack

		cat <<-SLACK
		{
		"blocks": [
			{
				"type": "section",
				"text": {
					"type": "mrkdwn",
					"text": "${slack_msg_header}"
				}
			},
			{
				"type": "divider"
			},
			{
				"type": "section",
				"text": {
					"type": "mrkdwn",
					"text": "${slack_msg_body}"
				}
			},
			{
				"type": "section",
				"fields": [
					{
						"type": "mrkdwn",
						"text": "*Environment*\n${ENVIRONMENT}"
					},
					{
						"type": "mrkdwn",
						"text": "*Pushed By:*\n${GITLAB_USER_NAME}"
					},
					{
						"type": "mrkdwn",
						"text": "*Commit Branch:*\n${CI_COMMIT_REF_NAME}"
					},
					{
						"type": "mrkdwn",
						"text": "*Comments:*\n${commit_msg}"
					},
					{
						"type": "mrkdwn",
						"text": "*Commit URL:*\nhttps://gitlab.com/javibjj/slack/-/commit/$(git rev-parse HEAD)"
					},
					{
						"type": "mrkdwn",
						"text": "*Job URL:*\nhttps://gitlab.com/javibjj/slack/-/jobs/${CI_JOB_ID}"
					},
					{
						"type": "mrkdwn",
						"text": "*Assigned To:*\n${assign}"
					}
				]
			},
			{
				"type": "divider"
			}
		]
	}

	SLACK
	}

###################################################################	

function share_slack_update() {
       curl -X POST                                           \
        --data-urlencode "payload=$(print_slack_summary)"  \
        "${SLACK_WEBHOOK}"               
}

share_slack_update

###################################################################
